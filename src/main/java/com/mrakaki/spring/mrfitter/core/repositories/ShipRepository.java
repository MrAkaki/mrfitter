package com.mrakaki.spring.mrfitter.core.repositories;

import java.util.List;

import com.mrakaki.spring.mrfitter.core.entities.Ship;

public interface ShipRepository {
	public List<Ship> getAllShips();
	
}
