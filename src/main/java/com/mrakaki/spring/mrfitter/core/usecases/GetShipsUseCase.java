package com.mrakaki.spring.mrfitter.core.usecases;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mrakaki.spring.mrfitter.core.entities.Ship;
import com.mrakaki.spring.mrfitter.core.entities.ShipFilter;
import com.mrakaki.spring.mrfitter.core.repositories.ShipRepository;

@Component
public class GetShipsUseCase {

	@Autowired
	private ShipRepository repository;

	public List<Ship> getAll() {
		return this.repository.getAllShips();
	}

	public List<Ship> getFiltered(final ShipFilter filter) {
		List<Ship> allShips = this.repository.getAllShips();

		return allShips.stream()
				.filter(ship -> ((filter.name.length() == 0 || ship.getName().contains(filter.name))
						&& (filter.empire.length() == 0 || ship.getEmpire().contains(filter.empire))
						&& (filter.classId.length() == 0 || ship.getClassid().equals(filter.classId))))
				.collect(Collectors.toList());
	}

	public Ship getById(final String id) {
		List<Ship> allShips = this.repository.getAllShips();
		Ship responce = null;
		for (Ship ship : allShips) {
			if (ship.getId().equals(id)) {
				responce = ship;
				break;
			}
		}
		return responce;
	}
}
