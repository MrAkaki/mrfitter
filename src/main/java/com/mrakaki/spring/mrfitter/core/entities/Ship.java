package com.mrakaki.spring.mrfitter.core.entities;

public class Ship {
	private String id;
	private String name;
	private String classid;
	private String empire;
	private int powergrid;
	private int cpu;
	
	public Ship(String id, String name, String classid, String empire, int powergrid, int cpu) {
		super();
		this.id = id;
		this.name = name;
		this.classid = classid;
		this.empire = empire;
		this.powergrid = powergrid;
		this.cpu = cpu;
	}
	
	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getClassid() {
		return classid;
	}
	
	public String getEmpire() {
		return empire;
	}
	
	public int getPowergrid() {
		return powergrid;
	}
	
	public int getCpu() {
		return cpu;
	}
	
	@Override
	public String toString() {
		return "Ship [id=" + id + ", name=" + name + ", classid=" + classid + ", empire=" + empire + ", powergrid="
				+ powergrid + ", cpu=" + cpu + "]";
	}
	
}
