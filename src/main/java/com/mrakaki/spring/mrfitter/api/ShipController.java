package com.mrakaki.spring.mrfitter.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mrakaki.spring.mrfitter.core.entities.Ship;
import com.mrakaki.spring.mrfitter.core.entities.ShipFilter;
import com.mrakaki.spring.mrfitter.core.usecases.GetShipsUseCase;


@RestController
@RequestMapping("/ship")
public class ShipController {
	
	@Autowired
	private GetShipsUseCase getShipsUseCase;
	
	@GetMapping("/")
	public List<Ship> getAllShips(){
		return this.getShipsUseCase.getAll();
	}
	
	@GetMapping("/{shipId}")
	public Ship getShip(@PathVariable(value="shipId") final String shipId){
		return this.getShipsUseCase.getById(shipId);
	}
	
	@PostMapping("/filter")
	public List<Ship> getShipsFiltered(@RequestBody final ShipFilter filter){
		return this.getShipsUseCase.getFiltered(filter);
	}

}
