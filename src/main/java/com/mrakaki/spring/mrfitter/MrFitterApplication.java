package com.mrakaki.spring.mrfitter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MrFitterApplication {

	public static void main(String[] args) {
		SpringApplication.run(MrFitterApplication.class, args);
	}

}
