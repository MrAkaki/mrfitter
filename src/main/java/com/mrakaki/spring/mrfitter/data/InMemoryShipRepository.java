package com.mrakaki.spring.mrfitter.data;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import com.mrakaki.spring.mrfitter.core.entities.Ship;
import com.mrakaki.spring.mrfitter.core.repositories.ShipRepository;

@Component
public class InMemoryShipRepository implements ShipRepository {

	final private List<Ship> ships = Arrays.asList(new Ship("aaaa-bbbb-cccc-dddd-eee","Orca","aaaaa-ccccc-dddd-ffff","Something",2,2));

	@Override
	public List<Ship> getAllShips() {
		return ships;
	}

}
